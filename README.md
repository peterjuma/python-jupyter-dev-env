# python-jupyter-dev-env

Use vagrant and Ansible to spin up a Python development environment with Jupyter GUI on a custom port


# Usage

Start up the machine:

`vagrant up`    

# Initialize an SSH tunnel
Initialize an SSH tunnel to the machine from the project folder to expose the Jupyter port (port 9090 as configured in the Ansible YAML file)

Note: The default port for Jupyter is 8888

# From a Linux host

* SSH Tunnel to Jupyter default port 8888 - (Accessed on host via localhost:9090)

`ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -l vagrant -i .vagrant/machines/python36/virtualbox/private_key -p 2222 -N -L 9090:localhost:8888 localhost`

* SSH Tunnel to Jupyter custom port 9090 - Accessed via localhost:9090

`ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -l vagrant -i .vagrant/machines/python36/virtualbox/private_key -p 2222 -N -L 9090:localhost:9090 localhost`

# From a Windows / Powershell host

* SSH Tunnel to Jupyter default port 8888 - (Accessed via localhost:9090)

`ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -l vagrant -i .\.vagrant\machines\python36\virtualbox\private_key -p 2222 -N -L 9090:localhost:8888 localhost`

* SSH Tunnel to Jupyter custom port 9090 - (Accessed via localhost:9090)

`ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -l vagrant -i .\.vagrant\machines\python36\virtualbox\private_key -p 2222 -N -L 9090:localhost:9090 localhost `


# Access Jupyter Web Browser

On the host machine, enter the URL below on a browser and use the given password which was set during provisioning with Ansible

```
URL - localhost:9090
Password: vagrant
```